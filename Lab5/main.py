import numpy as np


def getFile():
    text = []
    with open("file1", "r") as file:
        lines = file.readlines()
        for i in lines:
            text.append(i.replace('\n', ''))
    for i in range(len(text)):
        text[i] = text[i].split(" ")
    return text


class LinearModel:
    def __init__(self, A=np.empty([0, 0]), b=np.empty([0, 0]), c=np.empty([0, 0]), minmax="MAX"):
        self.A = A
        self.b = b
        self.c = c
        self.x = [float(0)] * len(c)
        self.minmax = minmax
        self.optimalValue = None
        self.transform = False

    def addA(self, A):
        self.A = A

    def addB(self, b):
        self.b = b

    def addC(self, c):
        self.c = c
        self.transform = False

    def setObj(self, minmax):
        if (minmax == "MIN" or minmax == "MAX"):
            self.minmax = minmax
        else:
            print("Invalid objective.")
        self.transform = False

    def printSoln(self):
        print("\nGot y: ")
        print(self.x)
        print("Objective function value: ")
        print(self.optimalValue)

    def getTableau(self):
        if (self.minmax == "MIN" and self.transform == False):
            self.c[0:len(c)] = -1 * self.c[0:len(c)]
            self.transform = True
        t1 = np.array([None, 0])
        numVar = len(self.c)
        numSlack = len(self.A)
        t1 = np.hstack(([None], [0], self.c, [0] * numSlack))
        basis = np.array([0] * numSlack)
        for i in range(0, len(basis)):
            basis[i] = numVar + i
        A = self.A
        if (not ((numSlack + numVar) == len(self.A[0]))):
            B = np.identity(numSlack)
            A = np.hstack((self.A, B))
        t2 = np.hstack((np.transpose([basis]), np.transpose([self.b]), A))
        tableau = np.vstack((t1, t2))
        tableau = np.array(tableau, dtype='float')
        
        return tableau

    def optimize(self):
        if self.minmax == "MIN" and self.transform == False:
            for i in range(len(self.c)):
                self.c[i] = -1 * self.c[i]
                transform = True
        tableau = self.getTableau()
        optimal = False
        iter = 1
    
        while(True):
            if (self.minmax == "MAX"):
                for profit in tableau[0, 2:]:
                    if profit > 0:
                        optimal = False
                        break
                    optimal = True
            else:
                for cost in tableau[0, 2:]:
                    if cost < 0:
                        optimal = False
                        break
                    optimal = True
            if optimal == True:
                break
            if (self.minmax == "MAX"):
                n = tableau[0, 2:].tolist().index(np.amax(tableau[0, 2:])) + 2
            else:
                n = tableau[0, 2:].tolist().index(np.amin(tableau[0, 2:])) + 2
            minimum = 99999
            r = -1
        
            for i in range(1, len(tableau)):
                if (tableau[i, n] > 0):
                    val = tableau[i, 1] / tableau[i, n]
                    if val < minimum:
                        minimum = val
                        r = i
        
            pivot = tableau[r, n]
            tableau[r, 1:] = tableau[r, 1:] / pivot
            for i in range(0, len(tableau)):
                if i != r:
                    mult = tableau[i, n] / tableau[r, n]
                    tableau[i, 1:] = tableau[i, 1:] - mult * tableau[r, 1:]
            tableau[r, 0] = n - 2
            iter += 1
    
    
        self.x = np.array([0] * len(c), dtype=float)
        for key in range(1, (len(tableau))):
            if (tableau[key, 0] < len(c)):
                self.x[int(tableau[key, 0])] = tableau[key, 1]
    
        self.optimalValue = -1 * tableau[0, 1]


if __name__ == '__main__':
    array = getFile()
    array = [[int(j) if '.' not in j else float(j) for j in i] for i in array]
    
    print('Matrix from file:')
    for i in array:
        for j in i:
            print(j, '\t', end='')
        print('')
    
    print("\tMinimax element of rows in row", ([min(i) for i in array]).index(max([min(i) for i in array]))+1)
    print("\tWith value", max([min(i) for i in array]), "\n")
    
    print("\tMaxmin element of columns in column",  ([max(i) for i in np.transpose(array)]).index(min([max(i) for i in np.transpose(array)]))+1)
    print("\tWith value", min([max(i) for i in np.transpose(array)]))
    
    if max([min(i) for i in array]) == min([max(i) for i in np.transpose(array)]):
        print("\nValues of strategy are equal")
    else:
        print("\nValues of strategy are not equal")
    
    print("\nResolve by linear model")
    linearModel = LinearModel()

    A = np.array(array)
    b = np.array([1 for i in range(len(array))])
    c = np.array([1 for i in range(len(array))])
    
    linearModel.addA(A)
    linearModel.addB(b)
    linearModel.addC(c)
    
    print("A=")
    for i in A:
        print(i)
    
    print("b =", b)
    print("c =", c)
    linearModel.optimize()
    linearModel.printSoln()
    
    y = linearModel.x
    x = [0, 1/28, 1/49, 3/7, 101/196]
    fx = linearModel.optimalValue
    g = 1/fx
    
    p = []
    q = []
    for i in range(len(x)):
        p.append(g * x[i])
        q.append(g * y[i])
    
    p.append(0)
    q.append(0)
    
    print("P:", p)
    print("Q:", q)
    