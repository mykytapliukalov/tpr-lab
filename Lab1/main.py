import numpy
from prettytable import PrettyTable


def getFile():
    text = []
    with open("file1", "r") as file:
        lines = file.readlines()
        for i in lines:
            text.append(i.replace('\n', ''))
    for i in range(len(text)):
        text[i] = text[i].split(" ")
    return text


def valda(array):
    answer = 0
    minArray = []
    
    minElem = numpy.max(array)
    maxMinElem = numpy.min(array)
    
    for i in array:
        for j in i:
            current = j
            if current < minElem:
                minElem = current
        if minElem > maxMinElem:
            answer = array.index(i) + 1
            maxMinElem = minElem
        minArray.append(minElem)
        minElem = numpy.max(array)
    return maxMinElem, answer, minArray


def hurvica(array, koef):
    answer = 0
    minArray = []
    maxArray = []
    resultsArray = []
    
    matrix = numpy.matrix(array)
    minElem = matrix.max()
    maxMinElem = matrix.min()
    result = matrix.min()
    
    for i in array:
        for j in i:
            if j > maxMinElem:
                maxMinElem = j
            else:
                minElem = j
        currentResult = koef * minElem + (1 - koef) * maxMinElem
        if currentResult > result:
            answer = array.index(i) + 1
            result = int(currentResult)
        minArray.append(minElem)
        maxArray.append(maxMinElem)
        resultsArray.append(int(currentResult))
        
        minElem = numpy.max(array)
        maxMinElem = numpy.min(array)
    
    return result, answer, minArray, maxArray, resultsArray


def laplasa(array):
    resultsArray = []
    
    for i in array:
        rowSum = 0
        for j in i:
            rowSum += j/len(i)
        resultsArray.append(round(rowSum, 2))
        
    answer = resultsArray.index(max(resultsArray)) + 1
    return max(resultsArray), answer, resultsArray


def bayesalaplasa(array, q1, q2, q3):
    resultsArray = []
    
    for i in array:
        resultsArray.append(i[0] * q1 + i[1] * q2 + i[2] * q3)
    
    answer = resultsArray.index(max(resultsArray)) + 1
    return max(resultsArray), answer, resultsArray


if __name__ == '__main__':
    array = getFile()

    print("Matrix:")
    table = PrettyTable(['Aij', 'П1', 'П2', 'П3'])
    for i in array:
        table.add_row([f'A{array.index(i)+1}', i[0], i[1], i[2]])
    print(table)
    
    array = [[int(j) for j in i] for i in array]
    answer, strategy, firstColumn = valda(array)
    print("\nValda:")
    table = PrettyTable(['Aj', 'П1', 'П2', 'П3', 'min(aij)'])
    for i in array:
        table.add_row([f'A{array.index(i) + 1}', i[0], i[1], i[2], firstColumn[array.index(i)]])
    print(table)
    print("\tResult:", answer)
    print("\tStrategy:", strategy)
    
    answer, strategy, firstColumn, secondColumn, thirdColumn = hurvica(array, 0.1)
    print("\nHurvica:")
    table = PrettyTable(['Aj', 'П1', 'П2', 'П3', 'min(aij)', 'max(aij)', 'H'])
    for i in array:
        table.add_row([f'A{array.index(i) + 1}', i[0], i[1], i[2],firstColumn[array.index(i)],
                       secondColumn[array.index(i)], thirdColumn[array.index(i)]])
    print(table)
    print("\tResult:", answer)
    print("\tStrategy:", strategy)
    
    answer, strategy, firstColumn = laplasa(array)
    print("\nLaplasa:")
    table = PrettyTable(['Aj', 'П1', 'П2', 'П3', 'L'])
    for i in array:
        table.add_row([f'A{array.index(i) + 1}', i[0], i[1], i[2], firstColumn[array.index(i)]])
    print(table)
    print("\tResult:", answer)
    print("\tStrategy:", strategy)
    
    answer, strategy, firstColumn = bayesalaplasa(array, 0.55, 0.3, 0.15)
    print("\nBayesa-Laplasa:")
    table = PrettyTable(['Aj', 'П1', 'П2', 'П3', 'eij'])
    for i in array:
        table.add_row([f'A{array.index(i) + 1}', i[0], i[1], i[2], firstColumn[array.index(i)]])
    print(table)
    print("\tResult:", answer)
    print("\tStrategy:", strategy)
